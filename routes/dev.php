<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dev Routes
|--------------------------------------------------------------------------
|
| THESE ROUTES ARE ONLY AVAILABLE FOR THE local ENVIRONMENT
|
| Routes are loaded by the RouteServiceProvider with the "dev" prefix.
| They are meant to aid in oAuth debug & development.
|
*/

Route::get('/redirect', function(\Illuminate\Http\Request $request) {
    $request->session()->put('state', $state = \Illuminate\Support\Str::random(40));

    $query = http_build_query([
        'client_id' => 'd6a4111e-72a7-4762-a596-cb4cad4a231b',
        'redirect_uri' => 'https://yourfestival-forms.fiona-app.com/account/external-authentication',
        'response_type' => 'code',
        'scope' => '*',
        'state' => $state,
    ]);

    return redirect('http://oauth.film-fest-gent.docksal/oauth/authorize?'.$query);
});

Route::get('/callback', function (\Illuminate\Http\Request $request) {
    $state = $request->session()->get('state');

    throw_unless(
        strlen($state) > 0 && $state === $request->state,
        InvalidArgumentException::class
    );

    $response = \Illuminate\Support\Facades\Http::asForm()->post('http://oauth.film-fest-gent.docksal/oauth/token', [
        'grant_type' => 'authorization_code',
        'client_id' => 'd6a4111e-72a7-4762-a596-cb4cad4a231b',
        'client_secret' => 'U1uxrEUG4tO8qD69UedQ1b0pNsDpeBobYXzKxrLq',
        'redirect_uri' => 'https://yourfestival-forms.fiona-app.com/account/external-authentication',
        'code' => $request->code,
    ]);

    return $response->json();
});
