<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fiona API Token
    |--------------------------------------------------------------------------
    |
    | This API token is used by Fiona to retrieve information about a specific
    | user without the need of the oAuth process.
    |
    */

    'token' => env('FIONA_API_TOKEN'),

    /*
    |--------------------------------------------------------------------------
    | oAuth fail redirect
    |--------------------------------------------------------------------------
    |
    | Redirect to the following page if the oAuth fails.
    |
    */

    'fail' => env('OAUTH_FAIL_REDIRECT', 'https://filmfestival.be/'),
];
