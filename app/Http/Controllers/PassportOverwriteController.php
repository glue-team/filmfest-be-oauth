<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Http\Controllers\AuthorizationController;
use Laravel\Passport\TokenRepository;
use Psr\Http\Message\ServerRequestInterface;
use PSR7SessionEncodeDecode\Decoder;

class PassportOverwriteController extends AuthorizationController
{

    /**
     * Authorize a client to access the user's account.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $psrRequest
     * @param \Illuminate\Http\Request                 $request
     * @param \Laravel\Passport\ClientRepository       $clients
     * @param \Laravel\Passport\TokenRepository        $tokens
     *
     * @return \Illuminate\Http\Response
     */
    public function authorize(ServerRequestInterface $psrRequest,
                                   Request $request,
                                   ClientRepository $clients,
                                   TokenRepository $tokens)
    {
        if($request->query->has('state')) {
            $state = $request->query->get('state');

            // Check if state is base64 encoded hash
            // https://stackoverflow.com/a/11154248
            if(preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $state)) {
                $redirectUrl = config('api.fail') . '?redirectUrl=' . base64_decode($request->query->get('state'));
            } else {
                $redirectUrl = config('api.fail') . '?redirectUrl=' . $request->query->get('state');
            }
        } else {
            $redirectUrl = config('api.fail');
        }

        if(! $request->hasCookie('PHPSESSID')) {
            return redirect($redirectUrl);
        }
        $session_id = $request->cookie('PHPSESSID');

        $now = Carbon::now();
        $php_session = DB::connection('craft')
            ->table('phpsessions')
            ->where('id', $session_id)
            ->where('expire', '>=', $now->getTimestamp())
            ->first();

        if(empty($php_session)) {
            return redirect($redirectUrl);
        }

        $data = (new Decoder)->__invoke($php_session->data);

        $token = array_values(array_filter(array_keys($data), function($key) {
            return ( strpos($key, 'token') !== false );
        }));
        $id = array_values(array_filter(array_keys($data), function($key) {
            return ( strpos($key, 'id') !== false );
        }));

        if(empty($token) || empty($id)) {
            return redirect($redirectUrl);
        }

        $token = $token[0];
        $id = $id[0];

        $craft_session = DB::connection('craft')
            ->table('sessions')
            ->where('token', $data[$token])
            ->where('userId', $data[$id])
            ->first();

        if(empty($craft_session)) {
            return redirect($redirectUrl);
        }

        Auth::loginUsingId($craft_session->userId);

        return parent::authorize($psrRequest, $request, $clients, $tokens);
    }
}
