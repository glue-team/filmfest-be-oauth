<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Str;
use yii\base\Security;

class CraftUserProvider implements UserProvider
{

    /**
     * The Eloquent user model.
     *
     * @var string
     */
    protected $model;

    /**
     * The Security service.
     *
     * @var Security
     */
    protected $securityService;

    /**
     * Create a new database user provider.
     *
     * @param User     $model
     * @param Security $security
     */
    public function __construct(User $model, Security $security)
    {
        $this->model = $model;
        $this->securityService = $security;
    }

    public function retrieveById($identifier)
    {
        return $this->newModelQuery()
            ->where($this->model->getAuthIdentifierName(), $identifier)
            ->first();
    }

    public function retrieveByToken($identifier, $token)
    {
        dd('token');
        // TODO: Implement retrieveByToken() method.
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        dd('update token');
        // TODO: Implement updateRememberToken() method.
    }

    public function retrieveByCredentials(array $credentials)
    {
        if(empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))) {
            return;
        }

        $query = $this->newModelQuery();

        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }

            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['password'];

        return $this->securityService->validatePassword($plain, $user->getAuthPassword());
    }

    protected function newModelQuery()
    {
        return $this->model->newQuery();
    }
}
