<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class CraftElement extends Authenticatable
{    protected $connection = 'craft';

    protected $table = 'elements';

}
