<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\CraftUser;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function me(Request $request)
    {
        return UserResource::make($request->user());
    }

    public function find($needle)
    {
        $user = CraftUser::whereHas('element', function ($query) use ($needle) {
            $query->where('uid', $needle);
        })
            ->orWhere('email', $needle)
            ->firstOrFail();

        return UserResource::make($user);
    }
}
