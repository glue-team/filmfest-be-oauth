<?php

namespace App\Providers;

use App\Http\Middleware\RequestLoggerMiddleware;
use App\Models\CraftUser;
use App\Passport\Client;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;
use yii\base\Security;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('craft', function() {
            return new CraftUserProvider(new CraftUser, new Security);
        });

        Route::group(['middleware' => RequestLoggerMiddleware::class], function() {
            Passport::routes();
        });

        Passport::useClientModel(Client::class);
    }
}
