<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'auth:api',
], function() {

    Route::get('/me', [UserController::class, 'me']);
});

Route::group([
    'middleware' => 'token',
    'prefix' => 'user',
], function() {

    Route::get('/{needle}', [UserController::class, 'find']);
});
