<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SwitchPassportToUuid extends Migration
{

    /**
     * The database schema.
     *
     * @var \Illuminate\Database\Schema\Builder
     */
    protected $schema;

    /**
     * Create a new migration instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->schema = Schema::connection($this->getConnection());
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->table('oauth_clients', function (Blueprint $table) {
            $table->uuid('id')->change();
        });

        $this->schema->table('oauth_auth_codes', function(Blueprint $table) {
            $table->uuid('client_id')->change();
        });

        $this->schema->table('oauth_access_tokens', function (Blueprint $table) {
            $table->uuid('client_id')->change();
        });

        $this->schema->table('oauth_personal_access_clients', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new Exception('There is no going back');
    }

    /**
     * Get the migration connection name.
     *
     * @return string|null
     */
    public function getConnection()
    {
        return config('passport.storage.database.connection');
    }
}
