<?php

use App\Http\Middleware\RequestLoggerMiddleware;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => RequestLoggerMiddleware::class], function() {

    // Overwrite the default Laravel Passport oAuth route
    Route::get('/oauth/authorize', [
        'uses' => 'PassportOverwriteController@authorize',
        'as'   => 'passport.authorizations.authorize',
    ]);
});
