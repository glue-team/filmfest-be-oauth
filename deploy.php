<?php
namespace Deployer;

require 'recipe/laravel.php';

// Overwrite default deploy
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

// Project name
set('application', 'Film Fest Gent oAuth');

// Project repository
set('repository', 'git@bitbucket.org:glue-team/filmfest-be-oauth.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('writable_mode', 'chmod');
set('writable_use_sudo', false);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

// Hosts
host('176.62.174.90')
    ->stage('master')
    ->user('filmfest_user')
    ->set('http_user', 'filmfest_user')
    ->set('deploy_path', '~/oauth.filmfestival.clients.glue.be')
    ->set('bin/php', '/opt/plesk/php/7.4/bin/php')
    ->set('bin/composer', '{{bin/php}} {{deploy_path}}/release/composer.phar');

// Tasks
task('custom:add_key_to_agent', function () {
    runLocally('ssh-add -k');
});

task('custom:add_known_host', function () {
    runLocally('ssh-keyscan -H {{hostname}} >> ~/.ssh/known_hosts');
    run('ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy:info', 'custom:add_key_to_agent');
after('deploy:info', 'custom:add_known_host');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

